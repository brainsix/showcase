<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
	<title>Showcase FTI UKDW</title>
	<link rel="stylesheet" type="text/css" href="Styles/styles.css">
	<script type="text/javascript" src="Javascript/script.js"></script>
</head>	
<body>
	<div id="menuUtama">
        <a class="dropbtn" href="home.php"><img class="icon-menu" alt="icon home" src="Images/home.png"><br>Home</a>
        <a class="dropbtn" href="profil.php"><img class="icon-menu" alt="icon profil" src="Images/profile.png"><br>Profile</a>
        <a class="dropbtn" href="lecturer-user.php"><img class="icon-menu" alt="icon lecturer" src="Images/lecturer.png"><br>Lecturer</a>
        <img id="gambarUkdw" alt="logo ukdw" src="Images/LogoUKDW.png">
        <div class="dropdown">
            <a class="dropbtn"><img class="icon-menu" alt="icon achievement" src="Images/achivment.png"><br>Achievement <span>&#9660;</span></a>
                <div class="dropdown-content  menu-hover">
                    <a class="dropdown-content-link" href="akademik-user.php">Academic</a>
                    <a class="dropdown-content-link" href="non-akademik-user.php">Non-academic</a>
                </div>
        </div>
        <div class="dropdown">
            <a class="dropbtn  current-page"><img class="icon-menu" alt="icon information" src="Images/information.png"><br>Information <span>&#9660;</span></a>
                <div class="dropdown-content  menu-hover">
                    <a class="dropdown-content-link  current-page" href="about-user.php">About UKDW</a>
                    <a class="dropdown-content-link" href="developer-user.php">Developer</a>
                </div>
        </div>
        <!--<form action="#"><input type="text" name="Search"><input type="submit" value="Search"></form>-->
        <div class="dropdown">
            <a onclick="myFunction()" class="dropbtn"><img class="icon-menu" alt="icon menu" src="Images/login.png"> <br>Hello, <em>username</em></a>
            <div id="myDropdown" class="dropdown-content">
                <a class="dropdown-content-link" href="index.php">Log out</a>
            </div>
        </div>
    </div>

    <div class="containerAbout">
        <div class="caseAchievement">
	    <div class="show">
			<hr class="AboutUs ">	
			<h1 id="about-center"> About UKDW </h1>
			<hr class="AboutUs"><br><br>
	    </div>
	    <p id="desc">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; English is a West Germanic language that was first spo ken in early medieval England and is now a global lingua franca.It is an official language of almost 60 sovereign states, the most commonly spoken language in the United Kingdom, the United States, Canada, Australia, Ireland, and New Zealand, and a widely spoken language in countries in the Caribbean, Africa, and South Asia. It is the third most common native language in the world, after Mandarin and Spanish. It is the most widely learned second language and is an official language of the United Nations, of the European Union, and of many other world and regional international organisations.

	    <br> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; English has developed over the course of more than 1,400 years. The earliest forms of English, a set of Anglo-Frisian dialects brought to Great Britain by Anglo-Saxon settlers in the fifth century, are called Old English. Middle English began in the late 11th century with the Norman conquest of England. Early Modern English began in the late 15th century with the introduction of the printing press to London and the King James Bible as well as the Great Vowel Shift. Through the worldwide influence of the British Empire, modern English spread around the world from the 17th to mid-20th centuries. Through all types of printed and electronic media, as well as the emergence of the United States as a global superpower, English has become the leading language of international discourse and the lingua franca in many regions and in professional contexts such as science, navigation, and law.
	    </p> <br><br>
        </div>
	    <a class="showLayout">
		<h3> UNIVERSITAS KRISTEN DUTA WACANA </h3>
		<p> Dr. Wahidin Sudiro Husodo Street Number 5 - 25 <br>
	 	 Yogyakarta 55224<br>
		 Contact : 0274 - 563929<br>
		 Fax	 : 0274 - 513235<br>
		 Email	 : step.love.brown.hani@gmail.com<br>
		</p>
		</a>
    </div>

</body>
</html>