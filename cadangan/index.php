<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
	<title>Showcase FTI UKDW</title>
	<link rel="stylesheet" type="text/css" href="Styles/styles.css">
	<script type="text/javascript" src="Javascript/script.js"></script>
</head>	
<body>
	<div id="menuUtama">
        <a class="dropbtn" ></a>
        <a class="dropbtn current-page" href="index.php"><img class="icon-menu" alt="icon home" src="Images/home.png"><br>Home</a>
        <a class="dropbtn" href="lecturer.php"><img class="icon-menu" alt="icon lecturer" src="Images/lecturer.png"><br>Lecturer</a>
        <img id="gambarUkdw" alt="logo ukdw" src="Images/LogoUKDW.png">
        <div class="dropdown">
            <a class="dropbtn"><img class="icon-menu" alt="icon achievement" src="Images/achivment.png"><br>Achievement <span>&#9660;</span></a>
                <div class="dropdown-content  menu-hover">
                    <a class="dropdown-content-link" href="akademik.php">Academic</a>
                    <a class="dropdown-content-link" href="non-akademik.php">Non-academic</a>
                </div>
        </div>
        <div class="dropdown">
            <a class="dropbtn"><img class="icon-menu" alt="icon information" src="Images/information.png"><br>Information <span>&#9660;</span></a>
                <div class="dropdown-content  menu-hover">
                    <a class="dropdown-content-link" href="about.php">About UKDW</a>
                    <a class="dropdown-content-link" href="developer.php">Developer</a>
                </div>
        </div>
        <!--<form action="#"><input type="text" name="Search"><input type="submit" value="Search"></form>-->
        <div class="dropdown">
        <a onclick="myFunction()" class="dropbtn"><img class="icon-menu" alt="icon login" src="Images/login.png"><br>Login</a>
            <div id="myDropdown" class="dropdown-content">
                <form name="form-login" action="proses-login.php" method="post" class="drop">
                    <input type="text" name="username" placeholder="Username" class="drop"><br>
                    <input type="password" name="password" placeholder="Password" class="drop"><br>
                    <input type="submit" value="Login">
                    <br>forgot password ?<a href="forgot-password.php" class="loginbtn">click here.</a>
                    <?php if($_REQUEST["error"] == 1) echo "username atau password salah"; ?>
                </form>
            </div>
        </div>
    </div>

   <div class="containerIndex">
        <img src="Images/mountains.jpg" alt="mountain" class="imgHome">
    <div class="case">
    <img src="Images/dota.png" alt="dota" class="sizefoto"><br>
        Posted by: Michael Natanael<br><br>
        Posted date: February 19, 2016<br><br>
        Category: Academic<br><br>
        Description: Defense of the Ancients (DotA) is a multiplayer online battle arena mod for the... <a href="description.php" class="link-normal">read more</a>
        </div>
    
    <div class="case">
    <img src="Images/gtaV.png" alt="gta" class="sizefoto"><br>
        Posted by: Michael Natanael<br><br>
        Posted date: February 23, 2016<br><br>
        Category: Academic<br><br>
        Description: Grand Theft Auto V is an open world, action-adventure video game developed by R... <a href="description.php" class="link-normal">read more</a>
    </div>
    
    <div class="case">
    <img src="Images/nba.jpg" alt="nba" class="sizefoto"><br>
        Posted by: Michael Natanael<br><br>
        Posted date: March 3, 2016<br><br>
        Category: Academic<br><br>
        Description: NBA 2K16 is a basketball simulation video game developed by Visual Concepts and pu... <a href="description.php" class="link-normal">read more</a>
    </div>
        
    <div class="case">
    <img src="Images/illuminati.jpg" alt="iluminati" class="sizefoto"><br>
        Posted by: Michael Natanael<br><br>
        Posted date: March 06, 2016<br><br>
        Category: Academic<br><br>
        Description: A few years back, after the release of his short film “Runaway,” rap impresario Kanye West found himself...  <a href="description.php" class="link-normal">read more</a>
    </div>
        
    <div class="case">
    <img src="Images/ctr.png" alt="ctr" class="sizefoto"><br>
        Posted by: Michael Natanael<br><br>
        Posted date: March 20, 2016<br><br>
        Category: Non-academic<br><br>
        Description: Crash Team Racing is a kart racing video game developed by Naughty Dog and publish... <a href="description.php" class="link-normal">read more</a>
    </div>
    </div>
    
</body>
</html>