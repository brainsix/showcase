<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
	<title>Showcase FTI UKDW</title>
	<link rel="stylesheet" type="text/css" href="Styles/styles.css">
	<script type="text/javascript" src="Javascript/script.js"></script>
</head>	
<body>
	<div id="menuUtama">
        <a class="dropbtn" ></a>
        <a class="dropbtn" href="index.php"><img class="icon-menu" alt="icon home" src="Images/home.png"><br>Home</a>
        <a class="dropbtn  current-page" href="lecturer.php"><img class="icon-menu" alt="icon lecturer" src="Images/lecturer.png"><br>Lecturer</a>
        <img id="gambarUkdw" alt="logo ukdw" src="Images/LogoUKDW.png">
        <div class="dropdown">
            <a class="dropbtn"><img class="icon-menu" alt="icon achievement" src="Images/achivment.png"><br>Achievement <span>&#9660;</span></a>
                <div class="dropdown-content  menu-hover">
                    <a class="dropdown-content-link" href="akademik.php">Academic</a>
                    <a class="dropdown-content-link" href="non-akademik.php">Non-academic</a>
                </div>
        </div>
        <div class="dropdown">
            <a class="dropbtn"><img class="icon-menu" alt="icon information" src="Images/information.png"><br>Information <span>&#9660;</span></a>
                <div class="dropdown-content  menu-hover">
                    <a class="dropdown-content-link" href="about.php">About UKDW</a>
                    <a class="dropdown-content-link" href="developer.php">Developer</a>
                </div>
        </div>
        <!--<form action="#"><input type="text" name="Search"><input type="submit" value="Search"></form>-->
        <div class="dropdown">
        <a onclick="myFunction()" class="dropbtn"><img class="icon-menu" alt="icon login" src="Images/login.png"><br>Login</a>
            <div id="myDropdown" class="dropdown-content">
                <form action="home.php" method="post" class="drop">
                    <input type="text" name="username" placeholder="Username" class="drop"><br>
                    <input type="password" name="password" placeholder="Password" class="drop"><br>
                    <input type="submit" value="Login">
                    <br>forgot password ?<a href="forgot-password.php" class="loginbtn">click here.</a>
                </form>
            </div>
        </div>
    </div>

	<div class="containerAbout">
    <div class="show">
        <div class="caseHalf">
		<div class="div-lecturer">
			<img class="img-lecturer" alt="icon lecturer" src="Images/lecturer.png"> 
			<p class="lecturer-content"><a href="http://lecturer.ukdw.ac.id/yuan/"> Yuan Lukito, S.Kom., M.Cs. </a><br><br>
			- Rekaya Perangkat Lunak, Web Development<br>
			- yuan@staff.ukdw.ac.id</p>
		</div>
            </div>
        <div class="caseHalf">
		<div class="div-lecturer">
			<img class="img-lecturer" alt="icon lecturer" src="Images/lecturer.png">
			<p class="lecturer-content"><a href="http://lecturer.ukdw.ac.id/anton/v2/index.php">Antonius Rachmat, S.Kom., M.Cs. </a><br><br>
		  		- Kepala Perpustakaan <br>
		  		- anton@staff.ukdw.ac.id</p>
		</div>
        </div>
        <div class="caseHalf">
		<div class="div-lecturer">
			<img class="img-lecturer" alt="icon lecturer" src="Images/lecturer.png"> 
			<p class="lecturer-content"><a href="http://lecturer.ukdw.ac.id/gunawan/"> Drs R. Gunawan Santoso, M.Si.</a><br><br>
			  	- Matematika dan Statistik <br>
				- gunawan@staff.ukdw.ac.id</p>
		</div>
        </div>
        <div class="caseHalf">
		<div class="div-lecturer">
			<img class="img-lecturer" alt="icon lecturer" src="Images/lecturer.png">
			<p class="lecturer-content"><a href="http://lecturer.ukdw.ac.id/cnuq/">
				Nugroho Agus Haryono, S.Si., MSi.</a><br><br>
		  		- Matematika dan Jaringan Komputer <br>
				- nugroho@staff.ukdw.ac.id</p>
		</div>
        </div>
        <div class="caseHalf">
		<div class="div-lecturer">
			<img class="img-lecturer" alt="icon lecturer" src="Images/lecturer.png"> 
			<p class="lecturer-content"><a href="http://lecturer.ukdw.ac.id/jokop/">Joko Purwadi, S.Kom., M.Kom.</a><br><br>
		  		- Kecerdasan Buatan dan Jaringan Komputer<br>
				- WR III<br>
				- jokop@staff.ukdw.ac.id</p>
            </div>
		</div>
        <div class="caseHalf">
		<div class="div-lecturer">
			<img class="img-lecturer" alt="icon lecturer" src="Images/lecturer.png">
			<p class="lecturer-content"><a href="http://lecturer.ukdw.ac.id/willysr/">Willy Sudiarto Raharjo, S.Kom, M.Cs.</a><br><br>
		  		- Rekayasa Perangkat Lunak dan Keamanan<br>
				- Wakil Dekan III (Bidang Kemahasiswaan)<br>
				- willysr@staff.ukdw.ac.id
            </p>
            </div>
		</div>
	  </div>
	  </div>
</body>
</html>