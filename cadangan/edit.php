<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
	<title>Showcase FTI UKDW</title>
	<link rel="stylesheet" type="text/css" href="Styles/styles.css">
	<script type="text/javascript" src="Javascript/script.js"></script>
</head>	
<body>
	<div id="menuUtama">
        <a class="dropbtn" href="home.php"><img class="icon-menu" alt="icon home" src="Images/home.png"><br>Home</a>
        <a class="dropbtn" href="profil.php"><img class="icon-menu" alt="icon profil" src="Images/profile.png"><br>Profile</a>
        <a class="dropbtn" href="lecturer-user.php"><img class="icon-menu" alt="icon lecturer" src="Images/lecturer.png"><br>Lecturer</a>
        <img id="gambarUkdw" alt="logo ukdw" src="Images/LogoUKDW.png">
        <div class="dropdown">
            <a class="dropbtn"><img class="icon-menu" alt="icon achievement" src="Images/achivment.png"><br>Achievement <span>&#9660;</span></a>
                <div class="dropdown-content  menu-hover">
                    <a class="dropdown-content-link" href="akademik-user.php">Academic</a>
                    <a class="dropdown-content-link" href="non-akademik-user.php">Non-academic</a>
                </div>
        </div>
        <div class="dropdown">
            <a class="dropbtn"><img class="icon-menu" alt="icon information" src="Images/information.png"><br>Information <span>&#9660;</span></a>
                <div class="dropdown-content  menu-hover">
                    <a class="dropdown-content-link" href="about-user.php">About UKDW</a>
                    <a class="dropdown-content-link" href="developer-user.php">Developer</a>
                </div>
        </div>
        <!--<form action="#"><input type="text" name="Search"><input type="submit" value="Search"></form>-->
        <div class="dropdown">
            <a onclick="myFunction()" class="dropbtn"><img class="icon-menu" alt="icon menu" src="Images/login.png"> <br>Hello, <em>username</em></a>
            <div id="myDropdown" class="dropdown-content">
                <a class="dropdown-content-link" href="index.php">Log out</a>
            </div>
        </div>
    </div>
	<div class="containerIndex">
        <div class="caseAchievement">
		<form action="profil.php" method="post">
                    Judul Post: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input id="judul-post" type="text" name="judul" placeholder="Judul Post" value="Defense of the Ancients"> <br><br>
                    <span >Deskripsi Post:&nbsp;&nbsp; </span><textarea id="isi-post" class="input-add" name="deskripsi" rows="10" cols="50" placeholder="masukan deskripsi post">Defense of the Ancients (DotA) is a multiplayer online battle arena mod for the video game Warcraft III: Reign of Chaos and its expansion, Warcraft III: The Frozen Throne, based on the "Aeon of Strife" map for StarCraft. The scenario objective is for each team to destroy the opponents' Ancient, heavily guarded structures at opposing corners of the map. Players use powerful units known as heroes, and are assisted by allied heroes and AI-controlled fighters. As in role-playing games, players level up their heroes and use gold to buy equipment during the mission.[1] The scenario was developed with the "World Editor" of Reign of Chaos, and was updated upon the release of its expansion, The Frozen Throne. There have been many variations of the original concept; the most popular being DotA Allstars, which eventually was simplified to DotA with the release of version 6.68.[2] This specific scenario has been maintained by several authors during development, the latest of whom is the anonymous developer known as "IceFrog" who has developed the game since 2005. Since its original release, DotA has become a feature at several worldwide tournaments, including Blizzard Entertainment's BlizzCon and the Asian World Cyber Games, as well as the Cyberathlete Amateur and CyberEvolution leagues; in a 2008 article of video game industry website Gamasutra, the article's author claimed that DotA was probably "the most popular and most-discussed free, non-supported game mod in the world".[3] DotA is largely attributed to being the most significant inspiration for the multiplayer online battle arena genre.[4] Valve Corporation acquired the intellectual property rights to DotA to develop a stand-alone sequel, Dota 2, which was released in 2013.[5][6]</textarea><br><br>
                    Upload gambar : <input type="file" accept="Images/dota-2-official.jpg"><br><br>
                    <input type="submit" value="Save">
		</form>
        </div>
	</div>

</body>
</html>