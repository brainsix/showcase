<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
	<title>Showcase FTI UKDW</title>
	<link rel="stylesheet" type="text/css" href="Styles/styles.css">
	<script type="text/javascript" src="Javascript/script.js"></script>
</head>	
<body>
	<div id="menuUtama">
        <a class="dropbtn" href="home.php"><img class="icon-menu" alt="icon home" src="Images/home.png"><br>Home</a>
        <a class="dropbtn  current-page" href="profil.php"><img class="icon-menu" alt="icon profil" src="Images/profile.png"><br>Profile</a>
        <a class="dropbtn" href="lecturer-user.php"><img class="icon-menu" alt="icon lecturer" src="Images/lecturer.png"><br>Lecturer</a>
        <img id="gambarUkdw" alt="logo ukdw" src="Images/LogoUKDW.png">
        <div class="dropdown">
            <a class="dropbtn"><img class="icon-menu" alt="icon achievement" src="Images/achivment.png"><br>Achievement <span>&#9660;</span></a>
                <div class="dropdown-content  menu-hover">
                    <a class="dropdown-content-link" href="akademik-user.php">Academic</a>
                    <a class="dropdown-content-link" href="non-akademik-user.php">Non-academic</a>
                </div>
        </div>
        <div class="dropdown">
            <a class="dropbtn"><img class="icon-menu" alt="icon information" src="Images/information.png"><br>Information <span>&#9660;</span></a>
                <div class="dropdown-content  menu-hover">
                    <a class="dropdown-content-link" href="about-user.php">About UKDW</a>
                    <a class="dropdown-content-link" href="developer-user.php">Developer</a>
                </div>
        </div>
        <!--<form action="#"><input type="text" name="Search"><input type="submit" value="Search"></form>-->
        <div class="dropdown">
            <a onclick="myFunction()" class="dropbtn"><img class="icon-menu" alt="icon menu" src="Images/login.png"> <br>Hello, <em>username</em></a>
            <div id="myDropdown" class="dropdown-content">
                <a class="dropdown-content-link" href="index.php">Log out</a>
            </div>
        </div>
    </div>
    <div class="containerIndex">
        <div class="caseAchievement">
    <img src="Images/bear.jpg" alt="bear" class="fotoProfile">
        <div class="isiAchievement">
        <form action="edit-biodata.php" method="post" class="btn-add"><input type="submit" value="Edit Biodata"></form>
        <a>Name: Michael Natanael</a><br>
        <a>Posts: 5</a><br>
        <a>Comments: 0</a><br><br>
        <a>Mobile Phone Number: 08123456789</a><br>
        <a>E-mail: 08123456789</a><br><br><br>
        <a>About Me: Michael Jeffrey Jordan (born February 17, 1963), also known by his initials, MJ, is an American former professional basketball player. He is also a businessman, and principal owner and chairman of the Charlotte Hornets. Jordan played 15 seasons in the National Basketball Association (NBA) for the Chicago Bulls and Washington Wizards. His biography on the NBA website states: "By acclamation, Michael Jordan is the greatest basketball player of all time." Jordan was one of the most effectively marketed athletes of his generation and was considered instrumental in popularizing the NBA around the world in the 1980s and 1990s</a>
        </div>
    </div>
    <div class="containerProduct">
    <div class="coba"><h4 class="post">Your Posts:</h4> <form action="add.php" method="post" class="btn-add"><input type="submit" value="Add Post"></form></div>
    <div class="caseProfile">
        <h3><a href="description-user.php">DotA</a></h3>
        <img src="Images/dota.png" alt="dota" class="sizefoto"><br>
        <a>Posted date: February 19, 2016</a><br><br>
        <a>Category: Academic</a><br><br>
        <a></a>
        <form action="edit.php" method="post" class="btnEdit">
            <input type="submit" value="Edit Post">
        </form>
    </div>
    <div class="caseProfile">
        <h3><a href="description-user.php">GTA V</a></h3>
        <img src="Images/gtaV.png" alt="gta" class="sizefoto"><br>
        <a>Posted date: March 23, 2016</a><br><br>
        <a>Category: Academic</a><br><br>
        <a></a>
        <form action="edit.php" method="post" class="btnEdit">
            <input type="submit" value="Edit Post">
        </form>
    </div>
    <div class="caseProfile">
        <h3><a href="description-user.php">NBA 2K16</a></h3>
        <img src="Images/nba.jpg" alt="nba" class="sizefoto"><br>
        <a>Posted date: March 3, 2016</a><br><br>
        <a>Category: Academic</a><br><br>
        <a></a>
        <form action="edit.php" method="post" class="btnEdit">
            <input type="submit" value="Edit Post">
        </form>
    </div>
    <div class="caseProfile">
        <h3><a href="description-user.php">Illuminati</a></h3>
        <img src="Images/illuminati.jpg" alt="iluminati" class="sizefoto"><br>
        <a>Posted date: March 3, 2016</a><br><br>
        <a>Category: Academic</a><br><br>
        <a></a>
        <form action="edit.php" method="post" class="btnEdit">
            <input type="submit" value="Edit Post">
        </form>
    </div>
    <div class="caseProfile">
        <h3><a href="description-user.php">CTR</a></h3>
        <img src="Images/ctr.png" alt="ctr" class="sizefoto"><br>
        <a>Posted date: March 20, 2016</a><br><br>
        <a>Category: Non-Academic</a><br><br>
        <a></a>
        <form action="edit.php" method="post" class="btnEdit">
            <input type="submit" value="Edit Post">
        </form>
    </div>
    </div>
    </div>
</body>
</html>