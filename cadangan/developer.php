<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
	<title>Showcase FTI UKDW</title>
	<link rel="stylesheet" type="text/css" href="Styles/styles.css">
	<script type="text/javascript" src="Javascript/script.js"></script>
</head>	
<body>
	<div id="menuUtama">
        <a class="dropbtn" ></a>
        <a class="dropbtn" href="index.php"><img class="icon-menu" alt="icon home" src="Images/home.png"><br>Home</a>
        <a class="dropbtn" href="lecturer.php"><img class="icon-menu" alt="icon lecturer" src="Images/lecturer.png"><br>Lecturer</a>
        <img id="gambarUkdw" alt="logo ukdw" src="Images/LogoUKDW.png">
        <div class="dropdown">
            <a class="dropbtn"><img class="icon-menu" alt="icon achievement" src="Images/achivment.png"><br>Achievement <span>&#9660;</span></a>
                <div class="dropdown-content  menu-hover">
                    <a class="dropdown-content-link" href="akademik.php">Academic</a>
                    <a class="dropdown-content-link" href="non-akademik.php">Non-academic</a>
                </div>
        </div>
        <div class="dropdown">
            <a class="dropbtn  current-page"><img class="icon-menu" alt="icon information" src="Images/information.png"><br>Information <span>&#9660;</span></a>
                <div class="dropdown-content  menu-hover">
                    <a class="dropdown-content-link" href="about.php">About UKDW</a>
                    <a class="dropdown-content-link current-page" href="developer.php">Developer</a>
                </div>
        </div>
        .php--<form action="#"><input type="text" name="Search"><input type="submit" value="Search"></form>-->
        <div class="dropdown">
        <a onclick="myFunction()" class="dropbtn"><img class="icon-menu" alt="icon login" src="Images/login.png"><br>Login</a>
            <div id="myDropdown" class="dropdown-content">
                <form action="home.php" method="post" class="drop">
                    <input type="text" name="username" placeholder="Username" class="drop"><br>
                    <input type="password" name="password" placeholder="Password" class="drop"><br>
                    <input type="submit" value="Login">
                    <br>forgot password ?<a href="forgot-password.php" class="loginbtn">click here.</a>
                </form>
            </div>
        </div>
    </div>
    <div class="containerAbout">
        <div class="caseAchievement">
    <img src="Images/bear.jpg" alt="bear image" class="fotoDev">
        <div class="isiAchievement">
        <a>Name: Michael Natanael</a><br><br>
        <a>Major: Informatics Engineering</a><br><br>
        <a>As: Coder</a><br><br>
        <a>Mobile Phone Number: 08123456789</a><br><br>
        <a>Facebook: facebook.com/MichaelNatanel.95</a><br><br>
        <a>E-mail: michael.natanael@ti.ukdw.ac.id</a><br><br><br>
        </div>
    </div>
    <div class="containerAbout">
        <div class="caseAchievement">
    <img src="Images/bear.jpg" alt="bear image" class="fotoDev">
        <div class="isiAchievement">
        <a>Name: Stephani Nugroho </a><br><br>
        <a>Major: Informatics Engineering</a><br><br>
        <a>As: Designer</a><br><br>
        <a>Mobile Phone Number: 08123456789</a><br><br>
        <a>Facebook: facebook.com/stephani.nugroho</a><br><br>
        <a>E-mail: stephani.nugroho@ti.ukdw.ac.id</a><br><br><br>
        </div>
    </div>
    <div class="containerAbout">
        <div class="caseAchievement">
    <img src="Images/bear.jpg" alt="bear image" class="fotoDev">
        <div class="isiAchievement">
        <a>Name: Jordi Riadi</a><br><br>
        <a>Major: Informatics Engineering</a><br><br>
        <a>As: Designer</a><br><br>
        <a>Mobile Phone Number: 08123456789</a><br><br>
        <a>Facebook: facebook.com/Jordiriadi</a><br><br>
        <a>E-mail: Jordy.riadil@ti.ukdw.ac.id</a><br><br><br>
        </div>
    </div>
    <div class="containerAbout">
        <div class="caseAchievement">
    <img src="Images/bear.jpg" alt="bear image" class="fotoDev">
        <div class="isiAchievement">
        <a>Name: Stefanus Indrawan</a><br><br>
        <a>Major: Informatics Engineering</a><br><br>
        <a>As: Coder</a><br><br>
        <a>Mobile Phone Number: 08123456789</a><br><br>
        <a>Facebook: facebook.com/JhonnyChrist.A7X</a><br><br>
        <a>E-mail: stefanus.indrawan@ti.ukdw.ac.id</a><br><br><br>
        </div>
    </div>
	</div>
        </div>
        </div>
    </div>
</body>
</html>