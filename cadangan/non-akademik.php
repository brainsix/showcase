<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
	<title>Showcase FTI UKDW</title>
	<link rel="stylesheet" type="text/css" href="Styles/styles.css">
	<script type="text/javascript" src="Javascript/script.js"></script>
</head>	
<body>
	<div id="menuUtama">
            <a class="dropbtn" ></a>
            <a class="dropbtn" href="index.php"><img class="icon-menu" alt="icon home" src="Images/home.png"><br>Home</a>
            <a class="dropbtn" href="lecturer.php"><img class="icon-menu" alt="icon lecturer" src="Images/lecturer.png"><br>Lecturer</a>
            <img id="gambarUkdw" alt="logo ukdw" src="Images/LogoUKDW.png">
            <div class="dropdown">
                <a class="dropbtn  current-page"><img class="icon-menu" alt="icon achievement" src="Images/achivment.png"><br>Achievement <span>&#9660;</span></a>
                    <div class="dropdown-content  menu-hover">
                        <a class="dropdown-content-link" href="akademik.php">Academic</a>
                        <a class="dropdown-content-link  current-page" href="non-akademik.php">Non-academic</a>
                    </div>
            </div>
            <div class="dropdown">
                <a class="dropbtn"><img class="icon-menu" alt="icon information" src="Images/information.png"><br>Information <span>&#9660;</span></a>
                    <div class="dropdown-content  menu-hover">
                        <a class="dropdown-content-link" href="about.php">About UKDW</a>
                        <a class="dropdown-content-link" href="developer.php">Developer</a>
                    </div>
            </div>
            <!--<form action="#"><input type="text" name="Search"><input type="submit" value="Search"></form>-->
            <div class="dropdown">
            <a onclick="myFunction()" class="dropbtn"><img class="icon-menu" alt="icon login" src="Images/login.png"><br>Login</a>
                <div id="myDropdown" class="dropdown-content">
                    <form action="home.php" method="post" class="drop">
                        <input type="text" name="username" placeholder="Username" class="drop"><br>
                        <input type="password" name="password" placeholder="Password" class="drop"><br>
                        <input type="submit" value="Login">
                        <br>forgot password ?<a href="forgot-password.php" class="loginbtn">click here.</a>
                    </form>
                </div>
            </div>
        </div>
        <div class="containerIndex">
    <div class="caseAchievement">
    <img src="Images/ctr.png" alt="ctr" class="sizefotoAchievement">
        <div class="isiAchievement">
        <h1><a href="description.php" class="clear">Crash Team Racing</a></h1>
        <a>Posted by: Michael Natanael</a><br><br>
        <a>Posted date: March 20, 2016</a><br><br>
        <a>Description: Crash Team Racing is a kart racing video game developed by Naughty Dog and published by Sony Computer Entertainment for the PlayStation. The game was released in North America on September 30, 1999 and in Europe and Australia later the same year. It was re-released for the Sony Greatest Hits line-up in 2000 and for the Platinum Range on January 12, 2001. It was later added to the European PlayStation Store on October 18, 2007, then on the Japanese store on June 11, 2008 and then finally to the North American store on August 10, 2010. It was also the final game in the series to be developed by Naughty Dog.</a>
        </div>
    </div>
</div>
</body>
</html>