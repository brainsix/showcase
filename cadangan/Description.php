<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
	<title>Showcase FTI UKDW</title>
	<link rel="stylesheet" type="text/css" href="Styles/styles.css">
	<script type="text/javascript" src="Javascript/script.js"></script>
</head>	
<body>
	<div id="menuUtama">
        <a class="dropbtn" ></a>
        <a class="dropbtn" href="index.php"><img class="icon-menu" alt="icon home" src="Images/home.png"><br>Home</a>
        <a class="dropbtn" href="lecturer.php"><img class="icon-menu" alt="icon lecturer" src="Images/lecturer.png"><br>Lecturer</a>
        <img id="gambarUkdw" alt="logo ukdw" src="Images/LogoUKDW.png">
        <div class="dropdown">
            <a class="dropbtn"><img class="icon-menu" alt="icon achievement" src="Images/achivment.png"><br>Achievement <span>&#9660;</span></a>
                <div class="dropdown-content  menu-hover">
                    <a class="dropdown-content-link" href="akademik.php">Academic</a>
                    <a class="dropdown-content-link" href="non-akademik.php">Non-academic</a>
                </div>
        </div>
        <div class="dropdown">
            <a class="dropbtn"><img class="icon-menu" alt="icon information" src="Images/information.png"><br>Information <span>&#9660;</span></a>
                <div class="dropdown-content  menu-hover">
                    <a class="dropdown-content-link" href="about.php">About UKDW</a>
                    <a class="dropdown-content-link" href="developer.php">Developer</a>
                </div>
        </div>
        <!--<form action="#"><input type="text" name="Search"><input type="submit" value="Search"></form>-->
        <div class="dropdown">
        <a onclick="myFunction()" class="dropbtn"><img class="icon-menu" alt="icon login" src="Images/login.png"><br>Login</a>
            <div id="myDropdown" class="dropdown-content">
                <form action="home.php" method="post" class="drop">
                    <input type="text" name="username" placeholder="Username" class="drop"><br>
                    <input type="password" name="password" placeholder="Password" class="drop"><br>
                    <input type="submit" value="Login">
                    <br>forgot password ?<a href="forgot-password.php" class="loginbtn">click here.</a>
                </form>
            </div>
        </div>
    </div>

	<div class="containerIndex">
        
        <div class="caseAchievement">
		<div id="desc">
			<h1>Defense of the Ancients</h1>
	        <p>Posted by: Michael Natanael<br><br>
	        Posted date: February 19, 2016</p>
			<hr>
			<br>
			<img class="desc-img" alt="dota 2 image" src="Images/dota-2-official.jpg" >
			<p>Defense of the Ancients (DotA) is a multiplayer online battle arena mod for the video game Warcraft III: Reign of Chaos and its expansion, Warcraft III: The Frozen Throne, based on the "Aeon of Strife" map for StarCraft. The scenario objective is for each team to destroy the opponents' Ancient, heavily guarded structures at opposing corners of the map. Players use powerful units known as heroes, and are assisted by allied heroes and AI-controlled fighters. As in role-playing games, players level up their heroes and use gold to buy equipment during the mission.
			<br><br>
			The scenario was developed with the "World Editor" of Reign of Chaos, and was updated upon the release of its expansion, The Frozen Throne. There have been many variations of the original concept; the most popular being DotA Allstars, which eventually was simplified to DotA with the release of version 6.68.This specific scenario has been maintained by several authors during development, the latest of whom is the anonymous developer known as "IceFrog" who has developed the game since 2005.
			<br><br>
			Since its original release, DotA has become a feature at several worldwide tournaments, including Blizzard Entertainment's BlizzCon and the Asian World Cyber Games, as well as the Cyberathlete Amateur and CyberEvolution leagues; in a 2008 article of video game industry website Gamasutra, the article's author claimed that DotA was probably "the most popular and most-discussed free, non-supported game mod in the world".DotA is largely attributed to being the most significant inspiration for the multiplayer online battle arena genre.Valve Corporation acquired the intellectual property rights to DotA to develop a stand-alone sequel, Dota 2, which was released in 2013</p>
		</div>
        </div>
	</div>
</body>
</html>