window.onload = function(){
 var judul =  document.getElementById('judul-post');
 var isi =  document.getElementById('isi-post');
 judul.onblur = checkJudul;
 isi.onblur = checkIsi;
}


/* When the user clicks on the button,
toggle between hiding and showing the dropdown content */
function myFunction() {
    document.getElementById("myDropdown").classList.toggle("show");
}

// Close the dropdown menu if the user clicks outside of it
window.onclick = function(event) {
  if (!event.target.matches('.dropbtn') && !event.target.matches('.drop')) {
    var dropdowns = document.getElementsByClassName("dropdown-content");
    var i;
    for (i = 0; i < dropdowns.length; i++) {
      var openDropdown = dropdowns[i];
      if (openDropdown.classList.contains('show')) {
        openDropdown.classList.remove('show');
      }
    }
  }
}

function checkJudul(){
  var judul = document.getElementById('judul-post')
  if(judul.value == ""){
    alert('Judul belum terisi')
    return false;
  }
}
function checkIsi(){
  var isi = document.getElementById('isi-post')
  if(isi.value == ""){
    alert('Deskripsi belum terisi')
    return false;
  }
}