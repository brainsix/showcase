<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
	<title>Showcase FTI UKDW</title>
	<link rel="stylesheet" type="text/css" href="Styles/styles.css">
	<script type="text/javascript" src="Javascript/script.js"></script>
</head>	
<body>
    <div id="menuUtama">
        <a class="dropbtn" href="home.php"><img class="icon-menu" alt="icon home" src="Images/home.png"><br>Home</a>
        <a class="dropbtn" href="profil.php"><img class="icon-menu" alt="icon profil" src="Images/profile.png"><br>Profile</a>
        <a class="dropbtn" href="lecturer-user.php"><img class="icon-menu" alt="icon lecturer" src="Images/lecturer.png"><br>Lecturer</a>
        <img id="gambarUkdw" alt="logo ukdw" src="Images/LogoUKDW.png">
        <div class="dropdown">
            <a class="dropbtn  current-page"><img class="icon-menu" alt="icon achievement" src="Images/achivment.png"><br>Achievement <span>&#9660;</span></a>
                <div class="dropdown-content  menu-hover">
                    <a class="dropdown-content-link  current-page" href="akademik-user.php">Academic</a>
                    <a class="dropdown-content-link" href="non-akademik-user.php">Non-academic</a>
                </div>
        </div>
        <div class="dropdown">
            <a class="dropbtn"><img class="icon-menu" alt="icon information" src="Images/information.png"><br>Information <span>&#9660;</span></a>
                <div class="dropdown-content  menu-hover">
                    <a class="dropdown-content-link" href="about-user.php">About UKDW</a>
                    <a class="dropdown-content-link" href="developer-user.php">Developer</a>
                </div>
        </div>
        <!--<form action="#"><input type="text" name="Search"><input type="submit" value="Search"></form>-->
        <div class="dropdown">
            <a onclick="myFunction()" class="dropbtn"><img class="icon-menu" alt="icon menu" src="Images/login.png"> <br>Hello, <em>username</em></a>
            <div id="myDropdown" class="dropdown-content">
                <a class="dropdown-content-link" href="index.php">Log out</a>
            </div>
        </div>
    </div>

    <div class="containerIndex">
        <div class="caseAchievement">
        <img src="Images/dota.png" alt="dota" class="sizefotoAchievement">
            <div class="isiAchievement">
            <h1><a href="description-user.php" class="clear">Defense of the Ancients</a></h1>
            <a>Posted by: Michael Natanael</a><br><br>
            <a>Posted date: February 19, 2016</a><br><br>
            <a>Description: Defense of the Ancients (DotA) is a multiplayer online battle arena mod for the video game Warcraft III: Reign of Chaos and its expansion, Warcraft III: The Frozen Throne, based on the "Aeon of Strife" map for StarCraft. The scenario objective is for each team to destroy the opponents' Ancient, heavily guarded structures at opposing corners of the map. Players use powerful units known as heroes, and are assisted by allied heroes and AI-controlled fighters. As in role-playing games, players level up their heroes and use gold to buy equipment during the mission.</a>
            </div>
        </div>

        <div class="caseAchievement">
        <img src="Images/gtaV.png" alt="gta" class="sizefotoAchievement">
            <div class="isiAchievement">
            <h1><a href="description-user.php" class="clear">Grand Theft Auto V</a></h1>
            <a>Posted by: Michael Natanael</a><br><br>
            <a>Posted date: February 23, 2016</a><br><br>
            <a>Description: Grand Theft Auto V is an open world, action-adventure video game developed by Rockstar North and published by Rockstar Games. It was released on 17 September 2013 for the PlayStation 3 and Xbox 360, on 18 November 2014 for the PlayStation 4 and Xbox One, and on 14 April 2015 for Microsoft Windows. The game is the first main entry in the Grand Theft Auto series since 2008's Grand Theft Auto IV. Set within the fictional state of San Andreas, based on Southern California, the single-player story follows three criminals and their efforts to commit heists while under pressure from a government agency. The open world design lets players freely roam San Andreas's open countryside and fictional city of Los Santos, based on Los Angeles.</a>
            </div>
        </div>

        <div class="caseAchievement">
        <img src="Images/nba.jpg" alt="nba" class="sizefotoAchievement">
            <div class="isiAchievement">
            <h1><a href="description-user.php" class="clear">NBA 2K16</a></h1>
            <a>Posted by: Michael Natanael</a><br><br>
            <a>Posted date: March 3, 2016</a><br><br>
            <a>Description: NBA 2K16 is a basketball simulation video game developed by Visual Concepts and published by 2K Sports. It is the 17th installment in the NBA 2K franchise and the successor to NBA 2K15. It was released on September 29, 2015 for Microsoft Windows, Xbox One, Xbox 360, PlayStation 4, and PlayStation 3. A mobile version for Android and iOS was also released on October 14, 2015. There are three different covers for the main game, one featuring Anthony Davis of the New Orleans Pelicans, another featuring Stephen Curry of the Golden State Warriors, and the last featuring James Harden of the Houston Rockets.</a>
            </div>
        </div>

        <div class="caseAchievement">
        <img src="Images/illuminati.jpg" alt="iluminati" class="sizefotoAchievement">
            <div class="isiAchievement">
            <h1><a href="description-user.php" class="clear">Illuminati</a></h1>
            <a>Posted by: jordi</a><br><br>
            <a>Posted date: March 06, 2016</a><br><br>
            <a>Description: A few years back, after the release of his short film “Runaway,” rap impresario Kanye West found himself busy quashing rumors that he practices devil worship and is part of the notorious Illuminati. The gossip was sparked by references in ”Runaway” to that elusive organization. West expressed confusion about the Illuminati. Is it connected to supposed devil worship? What does the word signify? The Illuminati refers to several different groups. The name derives from the Latin illuminatur, which means “enlightened.”</a>
            </div>
        </div>
            </div>
</body>
</html>