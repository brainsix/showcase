<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
	<title>Showcase FTI UKDW</title>
	<link rel="stylesheet" type="text/css" href="Styles/styles.css">
	<script type="text/javascript" src="Javascript/script.js"></script>
</head>	
<body>
    <div id="menuUtama">
        <a class="dropbtn" ></a>
        <a class="dropbtn" href="index.php"><img class="icon-menu" alt="icon home" src="Images/home.png"><br>Home</a>
        <div class="dropdown">
            <a class="dropbtn"><img class="icon-menu" alt="icon achievement" src="Images/achivment.png"><br>Achievement <span>&#9660;</span></a>
                <div class="dropdown-content  menu-hover">
                    <a class="dropdown-content-link" href="akademik.php">Academic</a>
                    <a class="dropdown-content-link" href="non-akademik.php">Non-academic</a>
                </div>
        </div>
        <img id="gambarUkdw" alt="logo ukdw" src="Images/LogoUKDW.png">
        
        <div class="dropdown">
            <a class="dropbtn current-page"><img class="icon-menu" alt="icon information" src="Images/information.png"><br>Information <span>&#9660;</span></a>
                <div class="dropdown-content  menu-hover">
                    <a class="dropdown-content-link" href="about.php">About UKDW</a>
                    <a class="dropdown-content-link" href="developer.php">Developer</a>
                    <a class="dropdown-content-link current-page" href="lecturer.php">Lecturer</a>
                </div>
        </div>
        <!--<form action="#"><input type="text" name="Search"><input type="submit" value="Search"></form>-->
        <div class="dropdown">
        <a onclick="myFunction()" class="dropbtn"><img class="icon-menu" alt="icon login" src="Images/login.png"><br>Login</a>
            <div id="myDropdown" class="dropdown-content">
                <form name="form-login" action="proses-login.php" method="post" class="drop">
                    <input type="text" name="username" placeholder="Username" class="drop"><br>
                    <input type="password" name="password" placeholder="Password" class="drop"><br>
                    <input type="submit" value="Login">
                    <br>forgot password ?<a href="forgot-password.php" class="loginbtn">click here.</a>
                    <?php if($_REQUEST["error"] == 1) echo "username atau password salah";?>
                </form>
            </div>
        </div>
        <a onclick="showSearch()"  class="dropbtn"><img class="icon-menu" alt="icon menu" src="Images/search.png"> <br>Search</a>
        <form style="display:none" action="search.php" id="search" method="GET"><input type="text" name="search"><input type="submit" name="submit" value="Search"></form>
    </div>

	<div class="containerAbout">
    <div class="show">
        <div class="caseHalf">
		<div class="div-lecturer">
			<img class="img-lecturer" alt="icon lecturer" src="Images/lecturer.png"> 
			<p class="lecturer-content"><a href="http://lecturer.ukdw.ac.id/yuan/"> Yuan Lukito, S.Kom., M.Cs. </a><br><br>
			- Rekaya Perangkat Lunak, Web Development<br>
			- yuan@staff.ukdw.ac.id</p>
		</div>
            </div>
        <div class="caseHalf">
		<div class="div-lecturer">
			<img class="img-lecturer" alt="icon lecturer" src="Images/lecturer.png">
			<p class="lecturer-content"><a href="http://lecturer.ukdw.ac.id/anton/v2/index.php">Antonius Rachmat, S.Kom., M.Cs. </a><br><br>
		  		- Kepala Perpustakaan <br>
		  		- anton@staff.ukdw.ac.id</p>
		</div>
        </div>
        <div class="caseHalf">
		<div class="div-lecturer">
			<img class="img-lecturer" alt="icon lecturer" src="Images/lecturer.png"> 
			<p class="lecturer-content"><a href="http://lecturer.ukdw.ac.id/gunawan/"> Drs R. Gunawan Santoso, M.Si.</a><br><br>
			  	- Matematika dan Statistik <br>
				- gunawan@staff.ukdw.ac.id</p>
		</div>
        </div>
        <div class="caseHalf">
		<div class="div-lecturer">
			<img class="img-lecturer" alt="icon lecturer" src="Images/lecturer.png">
			<p class="lecturer-content"><a href="http://lecturer.ukdw.ac.id/cnuq/">
				Nugroho Agus Haryono, S.Si., MSi.</a><br><br>
		  		- Matematika dan Jaringan Komputer <br>
				- nugroho@staff.ukdw.ac.id</p>
		</div>
        </div>
        <div class="caseHalf">
		<div class="div-lecturer">
			<img class="img-lecturer" alt="icon lecturer" src="Images/lecturer.png"> 
			<p class="lecturer-content"><a href="http://lecturer.ukdw.ac.id/jokop/">Joko Purwadi, S.Kom., M.Kom.</a><br><br>
		  		- Kecerdasan Buatan dan Jaringan Komputer<br>
				- WR III<br>
				- jokop@staff.ukdw.ac.id</p>
            </div>
		</div>
        <div class="caseHalf">
		<div class="div-lecturer">
			<img class="img-lecturer" alt="icon lecturer" src="Images/lecturer.png">
			<p class="lecturer-content"><a href="http://lecturer.ukdw.ac.id/willysr/">Willy Sudiarto Raharjo, S.Kom, M.Cs.</a><br><br>
		  		- Rekayasa Perangkat Lunak dan Keamanan<br>
				- Wakil Dekan III (Bidang Kemahasiswaan)<br>
				- willysr@staff.ukdw.ac.id
            </p>
            </div>
		</div>
	  </div>
	  </div>
</body>
</html>