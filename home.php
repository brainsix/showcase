<?php
    session_start();
    if(isset($_SESSION["USER"])&& $_SESSION["USER"]!="admin"){
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
	<title>Showcase FTI UKDW</title>
	<link rel="stylesheet" type="text/css" href="Styles/styles.css">
	<script type="text/javascript" src="Javascript/script.js"></script>
</head>	
<body>
	<div id="menuUtama">
        <a class="dropbtn current-page" href="home.php"><img class="icon-menu" alt="icon home" src="Images/home.png"><br>Home</a>
        <a class="dropbtn" href="profil.php"><img class="icon-menu" alt="icon profil" src="Images/profile.png"><br>Profile</a>
        <div class="dropdown">
            <a class="dropbtn"><img class="icon-menu" alt="icon achievement" src="Images/achivment.png"><br>Achievement <span>&#9660;</span></a>
                <div class="dropdown-content  menu-hover">
                    <a class="dropdown-content-link" href="akademik-user.php">Academic</a>
                    <a class="dropdown-content-link" href="non-akademik-user.php">Non-academic</a>
                </div>
            <img id="gambarUkdw" alt="logo ukdw" src="Images/LogoUKDW.png">
        </div>
        <div class="dropdown">
            <a class="dropbtn"><img class="icon-menu" alt="icon information" src="Images/information.png"><br>Information <span>&#9660;</span></a>
                <div class="dropdown-content  menu-hover">
                    <a class="dropdown-content-link" href="about-user.php">About UKDW</a>
                    <a class="dropdown-content-link" href="developer-user.php">Developer</a>
                    <a class="dropdown-content-link" href="lecturer-user.php">Lecturer</a>
                </div>
        </div>
        <!--<form action="#"><input type="text" name="Search"><input type="submit" value="Search"></form>-->
        <div class="dropdown">
            <a onclick="myFunction()" class="dropbtn"><img class="icon-menu" alt="icon menu" src="Images/login.png"> <br>Hello, <?php 
                $con = mysqli_connect("localhost","gn15a9","vL7t6xR3pz7B");
                $db = mysqli_select_db($con,"gn15a9");
                $query = "select nama from member where username = '". $_SESSION["USER"] ."'";
                $hasil = mysqli_query($con,$query);
                $baris = mysqli_fetch_array($hasil,MYSQLI_BOTH);
                echo $baris["nama"];
            ?></a>
            <div id="myDropdown" class="dropdown-content">
                <a class="dropdown-content-link" href="proses-logout.php">Log out</a>
            </div>
            
        </div>
        
            <a onclick="showSearch()"  class="dropbtn"><img class="icon-menu" alt="icon menu" src="Images/search.png"> <br>Search</a>
        <form style="display:none" action="search.php" id="search" method="GET"><input type="text" name="search"><input type="submit" name="submit" value="Search"></form>
    </div>
	
<div class="containerIndex">  
    <div class="container">
			<div class="image-slider-wrapper">
				<ul id="image_slider">
					<li><img src="Images/tide.jpg" alt="mountain" class="imgHome"></li>
					<li><img src="Images/radiant.jpg" alt="mountain" class="imgHome"></li>
					<li><img src="Images/tide2.jpg" alt="mountain" class="imgHome"></li>
					<li><img src="Images/background.png" alt="mountain" class="imgHome"></li>
					<li><img src="Images/wallpaper.jpg" alt="mountain" class="imgHome"></li>
					<li><img src="Images/mountains.jpg" alt="mountain" class="imgHome"></li>
				</ul>	
				<div class="pager">
				</div>
			</div>
		</div>
    <div class="containerView">
        <h3>Most Viewed Case</h3>
    <?php
            $con = mysqli_connect("localhost","gn15a9","vL7t6xR3pz7B");
            $db = mysqli_select_db($con,"gn15a9");
            $query = "select * from post order by view desc limit 4";
            $hasil = mysqli_query($con,$query);
            foreach ($hasil as $baris) {
                $query1 = "select nama from member where username ='".$baris["user"]."'";
                $hasil1 = mysqli_query($con,$query1);
                $baris1 = mysqli_fetch_array($hasil1,MYSQLI_BOTH);
                echo '<div class="case"><div class="info">Posted by: <a href="profil-user.php?user='.$baris["user"].'">'.$baris1["nama"].'</a><br><br>
                    Posted date: '.$baris["tanggal"].'<br><br>
                    Category: '.$baris["kategori"].'<br><br>
                    View(s): '.$baris["view"].'<br><br>
                    Description: '. substr($baris["deskripsi"], 0, 50) .'... <a href="Description.php?id='.$baris["id"].'" class="link-normal">read more</a>
                    </div>
                    <img src="Images/'.$baris["data"].'" alt="dota" class="sizefoto"><br>
                    
                    </div>';
            }
        ?> 
    
    
    </div>
    
    <?php
            $con = mysqli_connect("localhost","gn15a9","vL7t6xR3pz7B");
            $db = mysqli_select_db($con,"gn15a9");
            $query = "select * from post order by tanggal desc";
            $hasil = mysqli_query($con,$query);
            foreach ($hasil as $baris) {
                $query1 = "select nama from member where username ='".$baris["user"]."'";
                $hasil1 = mysqli_query($con,$query1);
                $baris1 = mysqli_fetch_array($hasil1,MYSQLI_BOTH);
                echo '<div class="case"><div class="info">Posted by: <a href="profil-user.php?user='.$baris["user"].'">'.$baris1["nama"].'</a><br><br>
                    Posted date: '.$baris["tanggal"].'<br><br>
                    Category: '.$baris["kategori"].'<br><br>
                    View(s): '.$baris["view"].'<br><br>
                    Description: '. substr($baris["deskripsi"], 0, 50) .'... <a href="Description.php?id='.$baris["id"].'" class="link-normal">read more</a>
                    </div>
                    <img src="Images/'.$baris["data"].'" alt="dota" class="sizefoto"><br>
                    
                    </div>';
            }
        ?>
    </div>
    
</body>
</html>
<?php
    }elseif ($_SESSION["USER"]=="admin") {
        header("location: admin.php");
}
    else header("Location: index.php");
?>