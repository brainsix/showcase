<?php
session_start();
unset($_SESSION["USER"]);
$_SESSION["LOG-IN"] = "false";
session_destroy();
header("Location: index.php");
?>