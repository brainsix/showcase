<?php
    session_start();
    if(isset($_SESSION["USER"])){
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
	<title>Showcase FTI UKDW</title>
	<link rel="stylesheet" type="text/css" href="Styles/styles.css">
	<script type="text/javascript" src="Javascript/script.js"></script>
</head>	
<body>
	<div id="menuUtama">
        <a class="dropbtn" href="home.php"><img class="icon-menu" alt="icon home" src="Images/home.png"><br>Home</a>
        <a class="dropbtn" href="profil.php"><img class="icon-menu" alt="icon profil" src="Images/profile.png"><br>Profile</a>
        <div class="dropdown">
            <a class="dropbtn"><img class="icon-menu" alt="icon achievement" src="Images/achivment.png"><br>Achievement <span>&#9660;</span></a>
                <div class="dropdown-content  menu-hover">
                    <a class="dropdown-content-link" href="akademik-user.php">Academic</a>
                    <a class="dropdown-content-link" href="non-akademik-user.php">Non-academic</a>
                </div>
            <img id="gambarUkdw" alt="logo ukdw" src="Images/LogoUKDW.png">
        </div>
        <div class="dropdown">
            <a class="dropbtn current-page"><img class="icon-menu" alt="icon information" src="Images/information.png"><br>Information <span>&#9660;</span></a>
                <div class="dropdown-content  menu-hover">
                    <a class="dropdown-content-link" href="about.php">About UKDW</a>
                    <a class="dropdown-content-link current-page" href="developer-user.php">Developer</a>
                    <a class="dropdown-content-link" href="lecturer-user.php">Lecturer</a>
                </div>
        </div>
        <!--<form action="#"><input type="text" name="Search"><input type="submit" value="Search"></form>-->
        <div class="dropdown">
            <a onclick="myFunction()" class="dropbtn"><img class="icon-menu" alt="icon menu" src="Images/login.png"> <br>Hello, <?php 
                $con = mysqli_connect("localhost","gn15a9","vL7t6xR3pz7B");
                $db = mysqli_select_db($con,"gn15a9");
                $query = "select nama from member where username = '". $_SESSION["USER"] ."'";
                $hasil = mysqli_query($con,$query);
                $baris = mysqli_fetch_array($hasil,MYSQLI_BOTH);
                echo $baris["nama"];
            ?></a>
            <div id="myDropdown" class="dropdown-content">
                <a class="dropdown-content-link" href="proses-logout.php">Log out</a>
            </div>
            
        </div>
        <a onclick="showSearch()"  class="dropbtn"><img class="icon-menu" alt="icon menu" src="Images/search.png"> <br>Search</a>
        <form style="display:none" action="search.php" id="search" method="GET"><input type="text" name="search"><input type="submit" name="submit" value="Search"></form>
    </div>
 <div class="containerAbout">
        <div class="caseAchievement">
    <img src="Images/bear.jpg" alt="bear image" class="fotoDev">
        <div class="isiAchievement">
        <a>Name: Michael Natanael</a><br><br>
        <a>Major: Informatics Engineering</a><br><br>
        <a>As: Coder</a><br><br>
        <a>Mobile Phone Number: 08123456789</a><br><br>
        <a>Facebook: facebook.com/MichaelNatanel.95</a><br><br>
        <a>E-mail: michael.natanael@ti.ukdw.ac.id</a><br><br><br>
        </div>
    </div>
        <div class="caseAchievement">
    <img src="Images/bear.jpg" alt="bear image" class="fotoDev">
        <div class="isiAchievement">
        <a>Name: Stephani Nugroho </a><br><br>
        <a>Major: Informatics Engineering</a><br><br>
        <a>As: Designer</a><br><br>
        <a>Mobile Phone Number: 08123456789</a><br><br>
        <a>Facebook: facebook.com/stephani.nugroho</a><br><br>
        <a>E-mail: stephani.nugroho@ti.ukdw.ac.id</a><br><br><br>
        </div>
    </div>
        <div class="caseAchievement">
    <img src="Images/bear.jpg" alt="bear image" class="fotoDev">
        <div class="isiAchievement">
        <a>Name: Jordi Riadi</a><br><br>
        <a>Major: Informatics Engineering</a><br><br>
        <a>As: Designer</a><br><br>
        <a>Mobile Phone Number: 08123456789</a><br><br>
        <a>Facebook: facebook.com/Jordiriadi</a><br><br>
        <a>E-mail: Jordy.riadil@ti.ukdw.ac.id</a><br><br><br>
        </div>
    </div>
        <div class="caseAchievement">
    <img src="Images/bear.jpg" alt="bear image" class="fotoDev">
        <div class="isiAchievement">
        <a>Name: Stefanus Indrawan</a><br><br>
        <a>Major: Informatics Engineering</a><br><br>
        <a>As: Coder</a><br><br>
        <a>Mobile Phone Number: 08123456789</a><br><br>
        <a>Facebook: facebook.com/JhonnyChrist.A7X</a><br><br>
        <a>E-mail: stefanus.indrawan@ti.ukdw.ac.id</a><br><br><br>
        </div>
    </div>
	</div>
</body>
</html>
<?php
    }elseif ($_SESSION["USER"]=="admin") {
        header("location: admin.php");
}
    else header("Location: index.php");
?>