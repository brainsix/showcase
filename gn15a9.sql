-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 17, 2016 at 01:23 AM
-- Server version: 10.1.9-MariaDB
-- PHP Version: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `gn15a9`
--

-- --------------------------------------------------------

--
-- Table structure for table `member`
--

CREATE TABLE `member` (
  `username` varchar(50) NOT NULL,
  `password` varchar(50) DEFAULT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `post` int(10) UNSIGNED DEFAULT '0',
  `phone` varchar(50) DEFAULT NULL,
  `email` varchar(150) DEFAULT NULL,
  `alamat` text,
  `about` text,
  `photo` text,
  `posponed` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='list user dan profil';

--
-- Dumping data for table `member`
--

INSERT INTO `member` (`username`, `password`, `nama`, `post`, `phone`, `email`, `alamat`, `about`, `photo`, `posponed`) VALUES
('71140003', '71140003', 'Michael Natanael', 0, '0123456789', 'ini.email@coba.com', 'jalan di jalan', 'hohohohoho', 'Desert.jpg', 2),
('admin', 'admin', 'admin', 0, NULL, NULL, NULL, NULL, NULL, 0),
('informatika', 'informatika', 'informatik', 5, '0987654321', 'informatika@ahahaha.com', 'jl. hahaha', 'saya adalah tester', 'Desert.jpg', 0);

-- --------------------------------------------------------

--
-- Table structure for table `post`
--

CREATE TABLE `post` (
  `id` int(10) UNSIGNED NOT NULL,
  `user` varchar(50) NOT NULL,
  `kategori` enum('Academic','Non-academic') NOT NULL,
  `judul` varchar(100) NOT NULL,
  `tanggal` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `deskripsi` text NOT NULL,
  `data` text NOT NULL,
  `status` enum('Y','N') NOT NULL DEFAULT 'N',
  `view` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `post`
--

INSERT INTO `post` (`id`, `user`, `kategori`, `judul`, `tanggal`, `deskripsi`, `data`, `status`, `view`) VALUES
(1, 'informatika', 'Academic', 'DoTA', '2016-05-14 19:40:07', 'Defense of the Ancients (DotA) is a multiplayer online battle arena mod for the video game Warcraft III: Reign of Chaos and its expansion, Warcraft III: The Frozen Throne, based on the "Aeon of Strife" map for StarCraft. The scenario objective is for each team to destroy the opponents'' Ancient, heavily guarded structures at opposing corners of the map. Players use powerful units known as heroes, and are assisted by allied heroes and AI-controlled fighters. As in role-playing games, players level up their heroes and use gold to buy equipment during the mission.', 'dota-2-official.jpg', 'Y', 8),
(2, 'informatika', 'Academic', 'Crash Team Racing', '2016-04-23 23:04:04', 'ini adalah CTR', 'ctr.png', 'Y', 0),
(10, 'informatika', 'Academic', 'COBA AH', '2016-04-23 17:00:00', 'coba masukin database post', 'Chaos_Theory.jpg', 'Y', 0),
(12, 'informatika', 'Non-academic', 'coba satu', '2016-05-15 02:41:52', 'coba non academic', 'Jellyfish.jpg', 'Y', 4),
(13, 'informatika', 'Non-academic', 'test', '2016-05-15 02:41:42', 'coba lagi masih gagal', 'Koala.jpg', 'Y', 1),
(14, '71140003', 'Academic', 'test 1', '2016-05-16 23:22:31', 'hahahaha', 'transformers-2-wallpaper-1.jpg', 'N', 0),
(15, '71140003', 'Non-academic', 'test 2', '2016-05-16 23:22:45', 'huhuhuhuhu', 'breaking-news2.jpg', 'N', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `member`
--
ALTER TABLE `member`
  ADD PRIMARY KEY (`username`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `post`
--
ALTER TABLE `post`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `post`
--
ALTER TABLE `post`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
