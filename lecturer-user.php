<?php
    session_start();
    if(isset($_SESSION["USER"])){
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
	<title>Showcase FTI UKDW</title>
	<link rel="stylesheet" type="text/css" href="Styles/styles.css">
	<script type="text/javascript" src="Javascript/script.js"></script>
</head>	
<body>
	<div id="menuUtama">
        <a class="dropbtn" href="home.php"><img class="icon-menu" alt="icon home" src="Images/home.png"><br>Home</a>
        <a class="dropbtn" href="profil.php"><img class="icon-menu" alt="icon profil" src="Images/profile.png"><br>Profile</a>
        <div class="dropdown">
            <a class="dropbtn"><img class="icon-menu" alt="icon achievement" src="Images/achivment.png"><br>Achievement <span>&#9660;</span></a>
                <div class="dropdown-content  menu-hover">
                    <a class="dropdown-content-link" href="akademik-user.php">Academic</a>
                    <a class="dropdown-content-link" href="non-akademik-user.php">Non-academic</a>
                </div>
            <img id="gambarUkdw" alt="logo ukdw" src="Images/LogoUKDW.png">
        </div>
        <div class="dropdown">
            <a class="dropbtn current-page"><img class="icon-menu" alt="icon information" src="Images/information.png"><br>Information <span>&#9660;</span></a>
                <div class="dropdown-content  menu-hover">
                    <a class="dropdown-content-link" href="about-user.php">About UKDW</a>
                    <a class="dropdown-content-link" href="developer-user.php">Developer</a>
                    <a class="dropdown-content-link current-page" href="lecturer-user.php">Lecturer</a>
                </div>
        </div>
        <!--<form action="#"><input type="text" name="Search"><input type="submit" value="Search"></form>-->
        <div class="dropdown">
            <a onclick="myFunction()" class="dropbtn"><img class="icon-menu" alt="icon menu" src="Images/login.png"> <br>Hello, <?php 
                $con = mysqli_connect("localhost","gn15a9","vL7t6xR3pz7B");
                $db = mysqli_select_db($con,"gn15a9");
                $query = "select nama from member where username = '". $_SESSION["USER"] ."'";
                $hasil = mysqli_query($con,$query);
                $baris = mysqli_fetch_array($hasil,MYSQLI_BOTH);
                echo $baris["nama"];
            ?></a>
            <div id="myDropdown" class="dropdown-content">
                <a class="dropdown-content-link" href="proses-logout.php">Log out</a>
            </div>
            
        </div>
        
            <a onclick="showSearch()"  class="dropbtn"><img class="icon-menu" alt="icon menu" src="Images/search.png"> <br>Search</a>
        <form style="display:none" action="search.php" id="search" method="GET"><input type="text" name="search"><input type="submit" name="submit" value="Search"></form>
    </div>
	<div class="containerAbout">
    <div class="show">
        <div class="caseHalf">
		<div class="div-lecturer">
			<img class="img-lecturer" alt="icon lecturer" src="Images/lecturer.png"> 
			<p class="lecturer-content"><a href="http://lecturer.ukdw.ac.id/yuan/"> Yuan Lukito, S.Kom., M.Cs. </a><br><br>
			- Rekaya Perangkat Lunak, Web Development<br>
			- yuan@staff.ukdw.ac.id</p>
		</div>
            </div>
        <div class="caseHalf">
		<div class="div-lecturer">
			<img class="img-lecturer" alt="icon lecturer" src="Images/lecturer.png">
			<p class="lecturer-content"><a href="http://lecturer.ukdw.ac.id/anton/v2/index.php">Antonius Rachmat, S.Kom., M.Cs. </a><br><br>
		  		- Kepala Perpustakaan <br>
		  		- anton@staff.ukdw.ac.id</p>
		</div>
        </div>
        <div class="caseHalf">
		<div class="div-lecturer">
			<img class="img-lecturer" alt="icon lecturer" src="Images/lecturer.png"> 
			<p class="lecturer-content"><a href="http://lecturer.ukdw.ac.id/gunawan/"> Drs R. Gunawan Santoso, M.Si.</a><br><br>
			  	- Matematika dan Statistik <br>
				- gunawan@staff.ukdw.ac.id</p>
		</div>
        </div>
        <div class="caseHalf">
		<div class="div-lecturer">
			<img class="img-lecturer" alt="icon lecturer" src="Images/lecturer.png">
			<p class="lecturer-content"><a href="http://lecturer.ukdw.ac.id/cnuq/">
				Nugroho Agus Haryono, S.Si., MSi.</a><br><br>
		  		- Matematika dan Jaringan Komputer <br>
				- nugroho@staff.ukdw.ac.id</p>
		</div>
        </div>
        <div class="caseHalf">
		<div class="div-lecturer">
			<img class="img-lecturer" alt="icon lecturer" src="Images/lecturer.png"> 
			<p class="lecturer-content"><a href="http://lecturer.ukdw.ac.id/jokop/">Joko Purwadi, S.Kom., M.Kom.</a><br><br>
		  		- Kecerdasan Buatan dan Jaringan Komputer<br>
				- WR III<br>
				- jokop@staff.ukdw.ac.id</p>
            </div>
		</div>
        <div class="caseHalf">
		<div class="div-lecturer">
			<img class="img-lecturer" alt="icon lecturer" src="Images/lecturer.png">
			<p class="lecturer-content"><a href="http://lecturer.ukdw.ac.id/willysr/">Willy Sudiarto Raharjo, S.Kom, M.Cs.</a><br><br>
		  		- Rekayasa Perangkat Lunak dan Keamanan<br>
				- Wakil Dekan III (Bidang Kemahasiswaan)<br>
				- willysr@staff.ukdw.ac.id
            </p>
            </div>
		</div>
	  </div>
	  </div>
</body>
</html>
<?php
    }elseif ($_SESSION["USER"]=="admin") {
        header("location: admin.php");
}
    else header("Location: index.php");
?>